module Refinery
  module PhotoAlbums
    class PhotoAlbum < Refinery::Core::BaseModel
      self.table_name = 'refinery_photo_albums'

      attr_accessible :title, :description, :position

      acts_as_indexed :fields => [:title, :description]
      has_many :images, :dependent => :destroy, :class_name => '::Refinery::Image'
      validates :title, :presence => true, :uniqueness => true
    end
  end
end
