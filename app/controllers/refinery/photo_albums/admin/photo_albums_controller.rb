module Refinery
  module PhotoAlbums
    module Admin
      class PhotoAlbumsController < ::Refinery::AdminController

        crudify :'refinery/photo_albums/photo_album', :xhr_paging => true

        def all_image
          @photo_album = Refinery::PhotoAlbums::PhotoAlbum.find(params[:photo_album_id])
          @images = @photo_album.images
        end

        def add_image
          @photo_album = Refinery::PhotoAlbums::PhotoAlbum.find(params[:photo_album_id])
          @image = @photo_album.images.build
        end

        def save_image
          @photo_album = Refinery::PhotoAlbums::PhotoAlbum.find(params[:photo_album_id])
          if params[:image][:image].present?
            params[:image][:image].each do |image|
              @image = @photo_album.images.create(:image => image)
            end
          else
            logger.debug("File nya nda ada")
          end
          redirect_to refinery.photo_albums_admin_photo_album_all_image_path(:photo_album_id => @photo_album.id)
        end

        def destroy_image
          @photo_album = Refinery::PhotoAlbums::PhotoAlbum.find(params[:photo_album_id])
          @image = Refinery::Image.find(params[:id])
          title = @image.image_name
          if @image.destroy
            flash.notice = t('destroyed', :scope => 'refinery.crudify', :what => "#{title}")
          end
          redirect_to refinery.photo_albums_admin_photo_album_all_image_path(@photo_album.id)
        end

      end
    end
  end
end
