module Refinery
  module PhotoAlbums
    class PhotoAlbumsController < ::ApplicationController

      before_filter :find_all_photo_albums
      before_filter :find_page

      def index
        # you can use meta fields from your model instead (e.g. browser_title)
        # by swapping @page for @photo_album in the line below:
        present(@page)
      end

      def show
        @photo_album = PhotoAlbum.find(params[:id])

        # you can use meta fields from your model instead (e.g. browser_title)
        # by swapping @page for @photo_album in the line below:
        present(@page)
      end

    protected

      def find_all_photo_albums
        @photo_albums = PhotoAlbum.order('position ASC')
      end

      def find_page
        @page = ::Refinery::Page.where(:link_url => "/photo_albums").first
      end

    end
  end
end
