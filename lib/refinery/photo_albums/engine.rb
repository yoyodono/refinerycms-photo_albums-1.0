module Refinery
  module PhotoAlbums
    class Engine < Rails::Engine
      include Refinery::Engine
      isolate_namespace Refinery::PhotoAlbums

      engine_name :refinery_photo_albums

      initializer "register refinerycms_photo_albums plugin" do
        Refinery::Plugin.register do |plugin|
          plugin.name = "photo_albums"
          plugin.url = proc { Refinery::Core::Engine.routes.url_helpers.photo_albums_admin_photo_albums_path }
          plugin.pathname = root
          plugin.activity = {
            :class_name => :'refinery/photo_albums/photo_album'
          }
          
        end
      end

      config.after_initialize do
        Refinery.register_extension(Refinery::PhotoAlbums)
      end
    end
  end
end
