
FactoryGirl.define do
  factory :photo_album, :class => Refinery::PhotoAlbums::PhotoAlbum do
    sequence(:title) { |n| "refinery#{n}" }
  end
end

