# encoding: utf-8
require "spec_helper"

describe Refinery do
  describe "PhotoAlbums" do
    describe "Admin" do
      describe "photo_albums" do
        login_refinery_user

        describe "photo_albums list" do
          before do
            FactoryGirl.create(:photo_album, :title => "UniqueTitleOne")
            FactoryGirl.create(:photo_album, :title => "UniqueTitleTwo")
          end

          it "shows two items" do
            visit refinery.photo_albums_admin_photo_albums_path
            page.should have_content("UniqueTitleOne")
            page.should have_content("UniqueTitleTwo")
          end
        end

        describe "create" do
          before do
            visit refinery.photo_albums_admin_photo_albums_path

            click_link "Add New Photo Album"
          end

          context "valid data" do
            it "should succeed" do
              fill_in "Title", :with => "This is a test of the first string field"
              click_button "Save"

              page.should have_content("'This is a test of the first string field' was successfully added.")
              Refinery::PhotoAlbums::PhotoAlbum.count.should == 1
            end
          end

          context "invalid data" do
            it "should fail" do
              click_button "Save"

              page.should have_content("Title can't be blank")
              Refinery::PhotoAlbums::PhotoAlbum.count.should == 0
            end
          end

          context "duplicate" do
            before { FactoryGirl.create(:photo_album, :title => "UniqueTitle") }

            it "should fail" do
              visit refinery.photo_albums_admin_photo_albums_path

              click_link "Add New Photo Album"

              fill_in "Title", :with => "UniqueTitle"
              click_button "Save"

              page.should have_content("There were problems")
              Refinery::PhotoAlbums::PhotoAlbum.count.should == 1
            end
          end

        end

        describe "edit" do
          before { FactoryGirl.create(:photo_album, :title => "A title") }

          it "should succeed" do
            visit refinery.photo_albums_admin_photo_albums_path

            within ".actions" do
              click_link "Edit this photo album"
            end

            fill_in "Title", :with => "A different title"
            click_button "Save"

            page.should have_content("'A different title' was successfully updated.")
            page.should have_no_content("A title")
          end
        end

        describe "destroy" do
          before { FactoryGirl.create(:photo_album, :title => "UniqueTitleOne") }

          it "should succeed" do
            visit refinery.photo_albums_admin_photo_albums_path

            click_link "Remove this photo album forever"

            page.should have_content("'UniqueTitleOne' was successfully removed.")
            Refinery::PhotoAlbums::PhotoAlbum.count.should == 0
          end
        end

      end
    end
  end
end
