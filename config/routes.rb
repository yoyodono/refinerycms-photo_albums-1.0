Refinery::Core::Engine.routes.append do

  # Frontend routes
  namespace :photo_albums do
    resources :photo_albums, :path => '', :only => [:index, :show]
  end

  # Admin routes
  namespace :photo_albums, :path => '' do
    namespace :admin, :path => 'refinery' do
      resources :photo_albums, :except => :show do
        get '/all_image' => 'photo_albums#all_image'
        get '/add_image' => 'photo_albums#add_image'
        post '/save_image' => 'photo_albums#save_image'
        delete '/destroy_image/:id' => 'photo_albums#destroy_image', :as => 'destroy_image'
        collection do
          post :update_positions
        end
      end
    end
  end

end
