class CreatePhotoAlbumsPhotoAlbums < ActiveRecord::Migration

  def up
    create_table :refinery_photo_albums do |t|
      t.string :title
      t.text :description
      t.integer :position

      t.timestamps
    end

  end

  def down
    if defined?(::Refinery::UserPlugin)
      ::Refinery::UserPlugin.destroy_all({:name => "refinerycms-photo_albums"})
    end

    if defined?(::Refinery::Page)
      ::Refinery::Page.delete_all({:link_url => "/photo_albums/photo_albums"})
    end

    drop_table :refinery_photo_albums

  end

end
