class AddPhotoAlbumToRefineryImages < ActiveRecord::Migration
  def change
    add_column :refinery_images, :photo_album_id, :integer
  end
end
